const app = require( 'express' )(),
    http = require( 'http' ).createServer( app ),
    io = require( 'socket.io' )( http ),
    jwt = require( 'jsonwebtoken' ),
    config = require( './core/config' ),
    game = require( './game' ),
    userService = require( './app/services/user.service' ),
    characterService = require( './app/services/character.service' );

http.listen( config.server.port );
console.log( `Game server listening on port ${config.server.port}` );

game.boot().then( () => {
    console.log( '[boot] Complete' );

    io.on( 'connection', socket => {
        console.log( 'Connected' );

        socket.on( 'authenticate token', token => {
            jwt.verify( token, config.secret, ( err, decoded_token ) => {
                if ( err ) {
                    return socket.emit( 'not authenticated', { message: 'Invalid credentials.' } );
                }

                userService.find( decoded_token.sub ).then( loadCharacter );
            } );
        } );

        socket.on( 'authenticate', data => {
            userService.authenticate( data )
                .then( loadCharacter )
                .catch( error => socket.emit( 'not authenticated', { message: 'Invalid credentials.' } ) );
        } );

        const loadCharacter = user => {
            characterService.load( user.id ).then( character => {
                if ( !character ) {
                    return socket.emit( 'not authenticated', { message: 'Character not found.' } );
                }

                game.connect( socket, character );
                socket.emit( 'authenticated', user );
            } );
        };
    } );
} ).catch( e => {
    console.log( '[boot] Error:', e );
} );
