const _ = require( 'lodash' ),
    db = require( '../../core/database' ),
    Item = require( '../models/item.model' );

module.exports = {
    boot,
    find,
    random,
};

let items = {};

async function boot() {
    return new Promise( ( resolve, reject ) => {
        db.query( 'SELECT * FROM items', function ( error, results ) {
            if ( error ) {
                return reject( error.sqlMessage || error );
            } else if ( !results[ 0 ] ) {
                return reject( 'No items found' );
            }

            _.forEach( results, item => items[ item.key ] = new Item( item ) );
            console.log( `[boot] item.service OK - ${_.size( items )} items loaded` );
            resolve();
        } );
    } );
}

function find( key ) {
    if ( items.hasOwnProperty( key ) ) {
        return items[ key ].get();
    }

    return {
        exists: false,
        type: 'item',
        key: key,
    };
}

function random() {
    return _.sample( items ).get();
}
