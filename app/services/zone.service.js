const _ = require( 'lodash' ),
    db = require( '../../core/database' ),
    Zone = require( '../models/zone.model' );

module.exports = {
    boot,
    find,
    enter,
};

let zones = {};

async function boot() {
    return new Promise( ( resolve, reject ) => {
        db.query( 'SELECT * FROM zones', function ( error, results ) {
            if ( error || !results[ 0 ] ) {
                return reject( error.sqlMessage || error );
            }

            _.forEach( results, zone => zones[ zone.key ] = new Zone( zone ) );
            console.log( `[boot] zone.service OK - ${_.size( zones )} zones loaded` );
            resolve();
        } );
    } );
}

function find( key ) {
    if ( zones.hasOwnProperty( key ) ) {
        return zones[ key ];
    }

    return {
        exists: false,
        type: 'zone',
        key: key,
    };
}

function canGoFrom( tile, newZone, character ) {
    if ( tile.quests ) {
        if ( tile.quests > character.quests.current ) {
            return false;
        }

        character.quests.current -= tile.quests;
    }

    return true;
}

function enter( tile, character, bypassChecks ) {
    const newZone = find( tile.to.key );

    if ( !bypassChecks && !canGoFrom( tile, newZone, character ) ) {
        return {
            data: { target: tile.to.key }
        };
    }

    character.zone = newZone.key;

    return {
        event: 'load zone',
        data: { zone: newZone },
    };
}
