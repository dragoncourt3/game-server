const _ = require( 'lodash' ),
    db = require( '../../core/database' ),
    Shop = require( '../models/shop.model' );

module.exports = {
    boot,
    find,
    enter,
};

let shops = {};

async function boot() {
    return new Promise( ( resolve, reject ) => {
        db.query( 'SELECT * FROM shops', function ( error, results ) {
            if ( error || !results[ 0 ] ) {
                return reject( error.sqlMessage || error );
            }

            _.forEach( results, shop => shops[ shop.key ] = new Shop( shop ) );
            console.log( `[boot] shop.service OK - ${_.size( shops )} shops loaded` );
            resolve();
        } );
    } );
}

function find( key ) {
    if ( shops.hasOwnProperty( key ) ) {
        return shops[ key ].get();
    }

    return {
        exists: false,
        type: 'shop',
        key: key,
    };
}

function enter( tile ) {
    return {
        event: 'load shop',
        data: { shop: find( tile.to.key ) },
    };
}
