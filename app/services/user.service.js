const jwt = require( 'jsonwebtoken' ),
    crypto = require( 'crypto' ),
    config = require( '../../core/config' ),
    db = require( '../../core/database' );

module.exports = {
    authenticate,
    find,
};

async function authenticate( { username, password } ) {
    return new Promise( (resolve, reject) => {
        db.query( {
            sql: 'SELECT * FROM users WHERE username = ? AND password = ?',
            values: [ username, hashPassword( password ) ]
        }, function ( error, results ) {
            if ( !results[ 0 ] ) {
                return reject( error );
            }

            const token = jwt.sign( { sub: results[ 0 ].id }, config.secret ),
                { password, ...userWithoutPassword } = results[ 0 ];

            resolve( {
                ...userWithoutPassword,
                token
            } );
        } );
    } );
}

async function find( id ) {
    return new Promise( ( resolve, reject ) => {
        db.query( {
            sql: 'SELECT * FROM users WHERE id = ?',
            values: [ id ]
        }, function ( error, results ) {
            if ( !results[ 0 ] ) {
                return reject( error );
            }

            const token = jwt.sign( { sub: results[ 0 ].id }, config.secret ),
                { password, ...userWithoutPassword } = results[ 0 ];

            resolve( {
                ...userWithoutPassword,
                token
            } );
        } );
    } );
}

function hashPassword( password ) {
    return crypto.createHash( 'sha256' )
        .update( password )
        .digest( 'base64' );
}
