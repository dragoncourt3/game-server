const _ = require( 'lodash' ),
    db = require( '../../core/database' ),
    monsters = require( './monsters.service' ),
    itemService = require( './item.service' );

module.exports = {
    boot,
    find,
    round,
    death,
};

const encounters = {};

async function boot() {
    return new Promise( ( resolve, reject ) => {
        db.query( 'SELECT * FROM encounters', function ( error, results ) {
            if ( error || !results[ 0 ] ) {
                return reject( error.sqlMessage || error );
            }

            _.forEach( results, encounter => {
                encounter = JSON.parse( JSON.stringify( encounter ) );

                encounter.exists = true;
                encounter.type = 'combat';
                encounter.descriptions = JSON.parse( encounter.descriptions );

                encounters[ encounter.key ] = encounter;
            } );

            console.log( `[boot] combat.service OK - ${_.size( encounters )} encounters loaded` );
            resolve();
        } );
    } );
}

function find( key, character ) {
    if ( !character.quests.current || !encounters.hasOwnProperty( key ) ) {
        return {
            data: { target: key },
        };
    }

    let fight = generate( _.clone( encounters[ key ] ) );
    character.encounter = fight;
    -- character.quests.current;

    return {
        event: 'load combat',
        data: { zone: fight },
    };
}

function generate( fight ) {
    fight.monster = monsters.select( fight.key );

    fight.description = _.sample( fight.descriptions )
        .replace( '{monster.name}', fight.monster.name );
    fight.reward = {
        exp: fight.monster.exp,
        marks: fight.monster.marks,
        items: [ itemService.random() ],
    };

    delete fight.descriptions;
    delete fight.monster.exp;
    delete fight.monster.marks;

    return fight;
}

function round( action, character ) {
    // @todo check if the encounter can be {action}ed

    if ( action === 'attack' ) {
        return attack( character );
    }

    if ( action === 'flee' ) {
        return flee( character );
    }

    if ( action === 'bribe' ) {
        return bribe( character );
    }

    if ( action === 'feed' ) {
        return feed( character );
    }

    return character;
}

function attack( character, canAttack = true ) {
    const monster = character.encounter.monster,
        damageDealt = calculateDamage( character.attack, monster.defence ),
        damageTaken = calculateDamage( monster.attack, character.defence );

    if ( canAttack ) {
        character.encounter.monster.guts -= damageDealt;
        character.notifications.push( `You dealt ${damageDealt} damage!` );
    }

    if ( character.encounter.monster.guts > 0 ) {
        character.guts.current -= damageTaken;
        character.notifications.push( `You took ${damageTaken} damage in return!` );

        if ( character.guts.current < 1 ) {
            character.notifications.push( 'Oh dear, you are dead!' );
        }
    } else {
        character.notifications.push( `The ${monster.name} falls to the floor, dead. You gain ${character.encounter.reward.exp} experience.` );
        character.notifications.push( `You search its body and find ${character.encounter.reward.marks} marks.` );
    }

    return character;
}

function calculateDamage( attack, defence ) {
    attack = _.random( 0, Math.max( 0, attack ) );
    defence = _.random( 1, Math.max( 1, defence ) );

    return Math.floor( attack * attack / (attack + defence) );
}

function flee( character ) {
    let successful = _.random( 1, 5 ) < 5; // 80%

    if ( successful ) {
        character.notifications.push( `You escape from the ${character.encounter.monster.name} as quickly as you can!` );

        if ( _.random( 1, 5 ) === 5 ) {
            character.notifications.push( 'As you flee, you hear it sniggering at you...' );
        }

        character.encounter = {};
    } else {
        character.notifications.push( `You try to run away, but the ${character.encounter.monster.name} catches you!` );

        if ( _.random( 1, 5 ) < 5 ) {
            character.notifications.push( 'It takes a cheap shot at you while you\'re running!' );
            attack( character, false );
        }
    }

    return character;
}

function bribe( character ) {
    // Have you got enough money?
    if ( character.marks < 25 ) {
        character.notifications.push( `You try to bribe the ${character.encounter.monster.name}, but it's insulted by how little you offered!` );

        character.notifications.push( 'It takes a swipe at you in disgust!' );
        attack( character, false );

        return character;
    }

    character.marks -= 25;
    character.encounter.reward.marks += 25;
    character.notifications.push( `The ${character.encounter.monster.name} thinks for a moment, then accepts your marks.` );

    let chance = 90; // % chance to succeed

    if ( _.random( 1, 100 ) > chance ) {
        character.notifications.push( '...but as you start to leave, it suddenly attacks you anyway!' );
        attack( character, false );
    } else {
        character.encounter = {};
    }

    return character;
}

function feed( character ) {
    return flee( character );
}

function death( character ) {
    _.forEach( [ 'guts', 'wits', 'charm' ], stat => {
        character[ stat ].current = character[ stat ].max;
    } );
    character.guts.current = 1;
    character.marks = Math.floor( character.marks / 2 );

    character.encounter = {};
    character.zone = 'fields';

    return character;
}
