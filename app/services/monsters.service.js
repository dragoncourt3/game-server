const _ = require( 'lodash' ),
    db = require( '../../core/database' ),
    Monster = require( '../models/monster.model' );

module.exports = {
    boot,
    select,
};

const population = {},
    monsters = {};

async function boot() {
    return Promise.all( [
        // Monsters
        new Promise( ( resolve, reject ) => {
            db.query( 'SELECT * FROM monsters', function ( error, results ) {
                if ( error || !results[ 0 ] ) {
                    return reject( error.sqlMessage || error );
                }

                _.forEach( results, monster => monsters[ monster.key ] = new Monster( monster ) );
                console.log( `[boot] monster.service OK - ${_.size( monsters )} monsters loaded` );
                resolve();
            } );
        } ),

        // Monsters in Encounters
        new Promise( ( resolve, reject ) => {
            db.query( 'SELECT * FROM encounters_monsters', function ( error, results ) {
                if ( error || !results[ 0 ] ) {
                    return reject( error.sqlMessage || error );
                }

                _.forEach( results, result => {
                    if ( !monsters.hasOwnProperty( result.monster_key ) ) {
                        return;
                    }

                    if ( !population.hasOwnProperty( result.encounter_key ) ) {
                        population[ result.encounter_key ] = [];
                    }

                    population[ result.encounter_key ].push( monsters[ result.monster_key ] );
                } );

                console.log( `[boot] monster.service OK - ${_.size( population )} encounters populated` );
                resolve();
            } );
        } )
    ] );
}

function select( zone ) {
    if ( population.hasOwnProperty( zone ) ) {
        return _.sample( population[ zone ] ).get();
    }

    return { exists: false };
}
