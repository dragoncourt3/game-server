const db = require( '../../core/database' ),
    Character = require( '../models/character.model' );

module.exports = {
    load,
    save,
};

async function load( id ) {
    return new Promise( ( resolve ) => {
        db.query( {
            sql: 'SELECT * FROM characters WHERE id = ?',
            values: [ id ]
        }, function ( error, results ) {
            if ( results === undefined || !results.length ) {
                console.log( 'Unknown character ID', id );
                resolve();
            } else {
                resolve( new Character( results[ 0 ] ) );
            }
        } );
    } );
}

async function save( character ) {
    return new Promise( ( resolve, reject ) => {
        db.query( {
            sql: 'UPDATE characters SET name = ?, rank = ?, level = ?, exp = ?, marks = ?, quests_current = ?, ' +
                'quests_max = ?, guts_current = ?, guts_max = ?, wits_current = ?, wits_max = ?, charm_current = ?, ' +
                'charm_max = ?, zone = ?, encounter = ? WHERE id = ?',
            values: [
                character.name, character.rank, character.level, character.exp, character.marks, character.quests.current,
                character.quests.max, character.guts.current, character.guts.max, character.wits.current, character.wits.max,
                character.charm.current, character.charm.max, character.zone, JSON.stringify( character.encounter ), character.id,
            ]
        }, function ( error ) {
            if ( error ) {
                return reject( error );
            }

            resolve();
        } );
    } );
}
