const characterService = require( '../services/character.service' ),
    combatService = require( '../services/combat.service' ),
    responseService = require( '../../core/services/response.service' ),
    shopService = require( '../services/shop.service' ),
    zoneService = require( '../services/zone.service' );

module.exports = function ( socket, character ) {
    this.socket = socket;
    this.character = character;

    this.listeners = {
        "combat action": combatAction.bind( this ),
        "resolve combat status": resolveCombatStatus.bind( this ),
    };
};

function combatAction( action ) {
    characterService.load( this.character.id ).then( character => {
        if ( !character.encounter.exists ) {
            return;
        }

        character = combatService.round( action, character );

        // Player dead?
        if ( character.guts.current < 1 ) {
            let merge = shopService.enter( { to: { key: 'fields.healer' } } ),
                response = { data: { character: combatService.death( character ) } };

            return responseService.merge( this.socket, character, response, merge );
        }

        // Monster dead?
        if ( character.encounter.monster && character.encounter.monster.guts < 1 ) {
            let merge = zoneService.enter( { to: { key: character.encounter.key.split( '.' )[ 0 ] } }, character, true );
            character.gainExp( character.encounter.reward.exp );
            character.gainMarks( character.encounter.reward.marks );
            character.encounter = {};

            return responseService.merge( this.socket, character, { data: { character } }, merge );
        }

        // Left the encounter somehow?
        if ( !character.encounter.exists ) {
            return responseService.currentZone( this.socket, character );
        }

        // Continue combat
        responseService.merge( this.socket, character, {
            event: 'load combat',
            data: {
                character: character,
                zone: character.encounter,
            },
        } );
    } );
}

function resolveCombatStatus() {
    characterService.load( character.id ).then( character => {
        if ( character.guts.current < 1 ) {
            let response = { data: { character: combatService.death( character ) } },
                merge = shopService.enter( { to: { key: 'fields.healer' } } );
            return responseService.merge( socket, character, response, merge );
        }

        if ( character.encounter.monster && character.encounter.monster.guts < 1 ) {
            let merge = zoneService.enter( { to: { key: character.encounter.key.split( '.' )[ 0 ] } }, character );
            character.encounter = {};
            character.marks += 10;
            return responseService.merge( socket, character, { data: { character } }, merge );
        }

        responseService.currentZone( socket, character );
    } );
}
