const characterService = require( '../services/character.service' ),
    combatService = require( '../services/combat.service' ),
    shopService = require( '../services/shop.service' ),
    zoneService = require( '../services/zone.service' ),
    responseService = require( '../../core/services/response.service' );

module.exports = function ( socket, character ) {
    this.socket = socket;
    this.character = character;

    this.listeners = {
        "click tile": clickTile.bind( this ),
        "reload zone": reloadZone.bind( this ),
    };
};

function reloadZone() {
    characterService.load( this.character.id )
        .then( character => responseService.currentZone( this.socket, character ) );
}

function clickTile( tile ) {
    // No running away!
    if ( this.character.encounter.exists ) {
        return;
    }

    characterService.load( this.character.id ).then( character => {
        const currentZone = zoneService.find( character.zone );

        let response = {
                event: 'invalid action',
                data: { character },
            },
            merge;

        if ( !currentZone.exists ) {
            return socket.emit( response.event, response.data );
        }

        tile = currentZone.tiles.find( t => t.position === tile );
        if ( !tile ) {
            return this.socket.emit( response.event, response.data );
        }

        // All tile type handlers here
        if ( tile.to.type === 'zone' ) {
            merge = zoneService.enter( tile, character );
        } else if ( tile.to.type === 'shop' ) {
            merge = shopService.enter( tile );
        } else if ( tile.to.type === 'combat' ) {
            merge = combatService.find( tile.to.key, character );
        }

        responseService.merge( this.socket, character, response, merge );
    } );
}
