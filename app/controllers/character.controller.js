const characterService = require( '../services/character.service' );

module.exports = function ( socket, character ) {
    this.socket = socket;
    this.character = character;

    this.listeners = {
        "show character": showCharacter.bind( this ),
    };
};

function showCharacter() {
    characterService.load( this.character.id )
        .then( character => this.socket.emit( 'load character', { character } ) );
}
