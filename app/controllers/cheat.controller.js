const _ = require( 'lodash' ),
    characterService = require( '../services/character.service' ),
    responseService = require( '../../core/services/response.service' );

module.exports = function ( socket, character ) {
    this.socket = socket;
    this.character = character;

    this.listeners = {
        "cheat full heal": fullHeal.bind( this ),
        "cheat refresh quests": refreshQuests.bind( this ),
    };
};

function fullHeal() {
    characterService.load( this.character.id ).then( character => {
        _.forEach( [ 'guts', 'wits', 'charm' ], stat => {
            character[ stat ].current = character[ stat ].max;
        } );

        responseService.currentZone( this.socket, character );
    } );
}

function refreshQuests() {
    characterService.load( this.character.id ).then( character => {
        character.quests.current = character.quests.max;

        responseService.currentZone( this.socket, character );
    } );
}
