const _ = require( 'lodash' );

module.exports = Item;

function Item( data = {} ) {

    const randomStats = [ 'attack', 'defence', 'skill' ];
    let statRanges = {};

    this.exists = !!data.id;
    this.type = 'item';

    if ( this.exists ) {
        this.id = parseInt( data.id );
        this.name = data.name;
        this.key = data.key;
        _.forEach( randomStats, ( range, stat ) => {
            statRanges[ stat ] = {
                min: data[ `${stat}_min` ],
                max: data[ `${stat}_max` ],
            };
        } );
    }

    this.get = () => {
        _.forOwn( statRanges, ( range, stat ) => {
            this[ stat ] = _.random( range.min, range.max );
        } );

        return this;
    };

}
