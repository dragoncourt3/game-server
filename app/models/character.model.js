const _ = require( 'lodash' );

module.exports = function ( data = {} ) {

    this.exists = !!data.id;

    if ( this.exists ) {
        this.id = parseInt( data.id );
        this.age = Math.ceil( (new Date() - new Date( data.created_at )) / (1000 * 60 * 60 * 24) );
        this.zone = data.zone;
        this.name = data.name;
        this.rank = data.rank;
        this.level = parseInt( data.level );
        this.exp = parseInt( data.exp );
        this.marks = parseInt( data.marks );
        this.items = JSON.parse( data.items || '[]' );
        this.quests = {
            current: parseInt( data.quests_current ),
            max: parseInt( data.quests_max ),
        };
        this.guts = {
            current: parseInt( data.guts_current ),
            max: parseInt( data.guts_max ),
        };
        this.wits = {
            current: parseInt( data.wits_current ),
            max: parseInt( data.wits_max ),
        };
        this.charm = {
            current: parseInt( data.charm_current ),
            max: parseInt( data.charm_max ),
        };
        this.gear = {
            weapon: 'Bare Fists',
            armour: '',
        };
        this.attack = parseInt( data.attack );
        this.defence = parseInt( data.defence );
        this.encounter = JSON.parse( data.encounter || '{}' );
        this.notifications = [];
    }

    this.gainExp = amount => {
        this.exp += amount;

        // https://stackoverflow.com/a/6955362/927652
        const level = Math.floor( (25 + Math.sqrt( 625 + 100 * this.exp )) / 50 );
        let gains = {};

        if ( level > this.level ) {
            const levelsGained = level - this.level;
            this.notifications.push( '*** LEVEL UP ' + (levelsGained > 1 ? `x${levelsGained} ` : '') + '***' );
            this.notifications.push( 'You grow stronger:' );

            while ( ++ this.level <= level ) {
                const levelGains = {
                    guts: _.random( 2, 5 ),
                    wits: _.random( 2, 5 ),
                    charm: _.random( 2, 5 ),
                    quests: level + 3,
                };

                _.forEach( levelGains, ( amount, stat ) => {
                    gains[ stat ] = (gains[ stat ] || 0) + amount;
                } );
            }

            _.forEach( gains, ( amount, stat ) => {
                this[ stat ].current += amount;
                this[ stat ].max += amount;
            } );

            this.notifications.push( `+${gains.guts} guts, +${gains.wits} wits, +${gains.charm} charm, +${gains.quests} quests` );
        }
    };

    this.gainMarks = amount => {
        this.marks += Math.abs( amount );
    }

};
