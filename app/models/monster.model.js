const _ = require( 'lodash' );

module.exports = function ( data = {} ) {

    const randomStats = [ 'guts', 'wits', 'charm', 'attack', 'defence', 'exp', 'marks' ];
    let statRanges = [];

    this.exists = !!data.id;

    if ( this.exists ) {
        this.id = parseInt( data.id );
        this.name = data.name;
        this.key = data.key;
        this.actions = [];
        _.forEach( [ 'attack', 'bribe', 'feed', 'flee', 'trade' ],
            action => data[ `action_${action}` ] && this.actions.push( action )
        );
        this.weapon = data.weapon;
        this.armour = data.armour;
        _.forEach( randomStats, stat => {
            statRanges[ stat ] = {
                min: parseInt( data[ `${stat}_min` ] ),
                max: parseInt( data[ `${stat}_max` ] ),
            };
        } );
    }

    this.get = () => {
        _.forOwn( statRanges, ( range, stat ) => {
            this[ stat ] = _.random( range.min, range.max );
        } );

        return this;
    }

};
