const _ = require( 'lodash' );

module.exports = function ( data = {} ) {

    let quotes = [];

    this.exists = !!data.id;
    this.type = 'shop';

    if ( this.exists ) {
        this.id = parseInt( data.id );
        this.name = data.name;
        this.key = data.key;
        this.character = data.character;
        quotes = JSON.parse( data.quotes );
    }

    this.get = () => {
        this.quote = _.sample( quotes );

        return this;
    }

};
