module.exports = function ( data = {} ) {

    this.exists = !!data.id;

    if ( this.exists ) {
        this.id = parseInt( data.id );
        this.name = data.name;
        this.key = data.key;
        this.type = 'zone';
        this.tiles = JSON.parse( data.tiles );
    }

};
