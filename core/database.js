const mysql = require( 'mysql' ),
    config = require( './config' );

let db = mysql.createConnection( config.database );
db.connect( err => err && console.log( 'MySQL error:', err ) );

module.exports = db;
