const _ = require( 'lodash' ),
    characterService = require( '../../app/services/character.service' ),
    zoneService = require( '../../app/services/zone.service' );

module.exports = {
    currentZone,
    merge,
};

function merge( socket, character, base, merge ) {
    merge = merge || {};
    base.event = merge.event || base.event;

    for ( let key in merge.data ) {
        if ( merge.data.hasOwnProperty( key ) ) {
            if ( base.data.hasOwnProperty( key ) ) {
                continue;
            }

            base.data[ key ] = merge.data[ key ];
        }
    }

    // console.log( response );
    return characterService.save( character )
        .then( () => socket.emit( base.event, base.data ) );
}

function currentZone( socket, character ) {
    return characterService.save( character ).then( () => {
        if ( character.encounter.exists ) {
            socket.emit( 'load combat', {
                character: character,
                zone: character.encounter,
            } );
        } else {
            socket.emit( 'load zone', {
                character: character,
                zone: zoneService.find( character.zone ),
            } );
        }
    } );
}
