<div class="row">
    <div class="col-9">
        <h1>Earl TestPlayer</h1>

        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div><strong>Level:</strong> 28</div>
                    <div><strong>Guts:</strong> 290</div>
                    <div><strong>Wits:</strong> 34</div>
                    <div><strong>Charm:</strong> 108</div>
                    <div><strong>Quests:</strong> 29</div>
                </div>
                <div class="col-6">
                    <div><strong>Age:</strong> 23</div>
                    <div><strong>Attack:</strong> 44</div>
                    <div><strong>Defend:</strong> 51</div>
                    <div><strong>Skill:</strong> 870</div>
                    <div>
                        <strong style="float: left; margin-right: 1em;">Exp:</strong>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 25%"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-3"><strong>Guild</strong></div>
                <div class="col-3"><strong>F:</strong> 15/15</div>
                <div class="col-3"><strong>M:</strong> 2/2</div>
                <div class="col-3"><strong>T:</strong> 5/5</div>
            </div>
        </div>
    </div>

    <div class="col-3">
        <div class="img-placeholder bt-0 bb-0" style="height: 184px;"></div>
    </div>
</div>

<hr class="m-0"/>

<div class="row">
    <div class="col-5">
        <h2>
            Backpack
            <small class="float-right mt-1 text-muted">(14/83)</small>
        </h2>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action">
                Rutter for Shangala
                <span class="badge badge-dark badge-pill">4</span>
            </a>
            <a class="list-group-item list-group-item-action">
                Thief Insurance
                <span class="badge badge-dark badge-pill">66</span>
            </a>
            <a class="list-group-item list-group-item-action">
                Camp Tent
            </a>
            <a class="list-group-item list-group-item-action">
                Cooking Gear
            </a>
            <a class="list-group-item list-group-item-action">
                Sleeping Bag
            </a>
            <a class="list-group-item list-group-item-action">
                Rope
                <span class="badge badge-dark badge-pill">183</span>
            </a>
            <a class="list-group-item list-group-item-action">
                Rutter for Hie Brasil
                <span class="badge badge-dark badge-pill">2</span>
            </a>
            <a class="list-group-item list-group-item-action">
                Marks
                <span class="badge badge-dark badge-pill">12,024,827</span>
            </a>
        </div>
    </div>

    <div class="col-7">
        <h2>Armaments</h2>
        <div class="container">
            <div class="row">
                <div class="col-8"></div>
                <div class="col-2">Atk</div>
                <div class="col-2">Def</div>
            </div>
            <div class="row">
                <div class="col-2"><strong>Head</strong></div>
                <div class="col-6">Miner's Cap</div>
                <div class="col-2">+1</div>
                <div class="col-2">+9</div>
            </div>
            <div class="row">
                <div class="col-2"><strong>Body</strong></div>
                <div class="col-6">Koutetsu</div>
                <div class="col-2">+6</div>
                <div class="col-2">+14</div>
            </div>
            <div class="row">
                <div class="col-2"><strong>Feet</strong></div>
                <div class="col-6">Sea Slippers</div>
                <div class="col-2">+1</div>
                <div class="col-2">+6</div>
            </div>
            <div class="row">
                <div class="col-2"><strong>Right</strong></div>
                <div class="col-6">Gladius</div>
                <div class="col-2">+16</div>
                <div class="col-2">+3</div>
            </div>
            <div class="row">
                <div class="col-2"><strong>Left</strong></div>
                <div class="col-6">Serpent Scale</div>
                <div class="col-2">+1</div>
                <div class="col-2">+9</div>
            </div>

            <div class="row mt-3">
                <div class="col-4">
                    <a class="btn btn-dark btn-block btn-sm text-white">Use</a>
                </div>
                <div class="col-4">
                    <a class="btn btn-dark btn-block btn-sm text-white">Info</a>
                </div>
                <div class="col-4">
                    <a class="btn btn-dark btn-block btn-sm text-white">Peer</a>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-4">
                    <a class="btn btn-dark btn-block btn-sm text-white">Drop</a>
                </div>
                <div class="col-4">
                    <a class="btn btn-dark btn-block btn-sm text-white">Pick up</a>
                </div>
                <div class="col-4">
                    <a href="?<?php echo str_replace( ':', '=', $_GET['back'] ) ?>"
                       class="btn btn-dark btn-block btn-sm text-white">
                        Exit
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
