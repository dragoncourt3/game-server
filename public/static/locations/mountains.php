<h1>High Crags of the Fenris Mountains</h1>

<section id="grid" class="container">
    <div class="row">
        <div class="col col-4">
            <a>Djinni's Magic</a>
        </div>

        <div class="col col-4">
            <a href="?location=mountains">(1) Quest</a>
        </div>

        <div class="col col-4">
            <a>Gem Exchange</a>
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            <a>Camp</a>
        </div>

        <div class="col col-4">
            <a>Abandoned Mine</a>
        </div>

        <div class="col col-4">
            <a>(1) Forest Trail</a>
        </div>
    </div>
</section>
