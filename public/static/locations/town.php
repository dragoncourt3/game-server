<h1>Main Street in Salamander Townshippe</h1>

<section id="grid" class="container">
    <div class="row">
        <div class="col col-4">
            <a>Keeper's Tavern</a>
        </div>

        <div class="col col-4">
            <a href="?location=market">Market Lane</a>
        </div>

        <div class="col col-4">
            <a href="?location=castle">(1) Castle Gate [@ lv6]</a>
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            <a>Smiths Weapons</a>
        </div>

        <div class="col col-4">
            <a>Trade Shoppe</a>
        </div>

        <div class="col col-4">
            <a href="?location=fields">Leave Towne</a>
        </div>
    </div>
</section>
