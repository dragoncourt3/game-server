<h1>Welcome to the Fields</h1>

<section id="grid" class="container">
    <div class="row">
        <div class="col col-4">
            <a href="?location=town">Towne Road</a>
        </div>

        <div class="col col-4">
            <a>(1) Quest</a>
        </div>

        <div class="col col-4">
            <a>Healer's Tower</a>
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            <a>(1) Forest Road [@ lv4]</a>
        </div>

        <div class="col col-4">
            <a>(1) Goblin Mound</a>
        </div>

        <div class="col col-4">
            <a>Camp</a>
        </div>
    </div>
</section>
