<h1>The Depths of the Arcane Forest</h1>

<section id="grid" class="container">
    <div class="row">
        <div class="col col-4">
            <a href="?location=mountains">(1) Mountain Trail</a>
        </div>

        <div class="col col-4">
            <a>(1) Quest</a>
        </div>

        <div class="col col-4">
            <a href="?location=fields">(1) Fields Road</a>
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            Shortlegs Smithy
        </div>

        <div class="col col-4">
            <a>Camp</a>
        </div>

        <div class="col col-4">
            The Guild
        </div>
    </div>
</section>
