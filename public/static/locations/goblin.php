<h1>Welcome to the Goblin Mound</h1>

<section id="grid" class="container">
    <div class="row">
        <div class="col col-4">
            <a href="?location=fields">To the Fields</a>
        </div>

        <div class="col col-4">
            <a>(1) Dark Vortex</a>
        </div>

        <div class="col col-4">
            <a>Gobble Inn</a>
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            <a>(1) Throne Room</a>
        </div>

        <div class="col col-4">
            <a>(1) Warrens</a>
        </div>

        <div class="col col-4">
            <a>(1) Treasury</a>
        </div>
    </div>
</section>
