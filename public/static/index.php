<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<main>
    <?php
    if ( isset( $_GET['location'] ) ) {
        $screen = 'locations/' . $_GET['location'];
        $path   = 'location:' . $_GET['location'];
    } else if ( isset( $_GET['screen'] ) ) {
        $screen = $_GET['screen'];
        $path   = 'screen:' . $_GET['screen'];
    } else {
        $screen = 'locations/fields';
        $path   = 'location:fields';
    }

    $inCharacterStats = $path === 'screen:character/stats';

    require "./$screen.php";
    ?>
    <footer>
        <div class="row pl-2 no-gutters">
            <div class="col-11 mt-2">
                <div class="row">
                    <div class="col-3"><strong>Earl TestPlayer</strong></div>
                    <div class="col-2"><strong>Guts:</strong> 290</div>
                    <div class="col-2"><strong>Wits:</strong> 34</div>
                    <div class="col-2"><strong>Charm:</strong> 108</div>
                    <div class="col-3"><strong>Marks:</strong> 12,024,827</div>
                </div>
                <div class="row">
                    <div class="col-3"><strong>Quests:</strong> 29</div>
                    <div class="col-2"><strong>Level:</strong> 28</div>
                    <div class="col-2"><strong>Exp:</strong> 234,226</div>
                    <div class="col-5">Gladius &amp; Koutetsou</div>
                </div>
            </div>
            <div class="col-1">
                <a class="btn btn-block btn-dark"
                   href="?<?php echo $inCharacterStats ? str_replace( ':', '=', $_GET['back'] ) : "screen=character/stats&amp;back=$path"?>">
                    <i class="fas fa-fw <?php echo $inCharacterStats ? 'fa-globe-americas' : 'fa-walking' ?> fa-flip-horizontal text-white"></i>
                </a>
            </div>
        </div>
    </footer>
</main>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
