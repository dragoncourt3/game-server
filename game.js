const _ = require( 'lodash' ),
    characterService = require( './app/services/character.service' ),
    combatService = require( './app/services/combat.service' ),
    itemService = require( './app/services/item.service' ),
    monsterService = require( './app/services/monsters.service' ),
    shopService = require( './app/services/shop.service' ),
    zoneService = require( './app/services/zone.service' ),
    responseService = require('./core/services/response.service'),
    CharacterController = require( './app/controllers/character.controller' ),
    CheatController = require( './app/controllers/cheat.controller' ),
    CombatController = require( './app/controllers/combat.controller' ),
    ZoneController = require( './app/controllers/zone.controller' );

module.exports = {
    boot,
    connect
};

function boot() {
    return Promise.all( [
        combatService.boot(),
        itemService.boot(),
        monsterService.boot(),
        shopService.boot(),
        zoneService.boot(),
    ] );
}

function connect( socket, character ) {
    console.log( 'Character ' + character.id + ' logged in' );

    const controllers = [
        new CharacterController( socket, character ),
        new CheatController( socket, character ),
        new CombatController( socket, character ),
        new ZoneController( socket, character ),
    ];

    _.forEach( controllers, controller => {
        _.forEach( controller.listeners, ( listener, event ) => {
            socket.on( event, listener );
        } );
    } );

    socket.on( 'load', () => {
        characterService.load( character.id )
            .then( character => responseService.currentZone( socket, character ) );
    } );

    socket.on( 'disconnect', reason => {
        if ( character ) {
            console.log( 'User', character.id, 'disconnected:', reason );
        } else {
            console.log( 'Anonymous user disconnected:', reason );
        }
    } );
}
