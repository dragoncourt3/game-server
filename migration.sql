# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.38)
# Database: node
# Generation Time: 2019-06-28 19:46:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table characters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `characters`;

CREATE TABLE `characters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL DEFAULT 'fields',
  `name` varchar(255) NOT NULL DEFAULT '',
  `rank` varchar(255) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT '1',
  `exp` int(11) NOT NULL DEFAULT '0',
  `marks` int(11) NOT NULL DEFAULT '0',
  `items` text NOT NULL,
  `quests_current` int(11) NOT NULL DEFAULT '100',
  `quests_max` int(11) NOT NULL DEFAULT '100',
  `guts_current` int(11) NOT NULL DEFAULT '10',
  `guts_max` int(11) NOT NULL DEFAULT '10',
  `wits_current` int(11) NOT NULL DEFAULT '10',
  `wits_max` int(11) NOT NULL DEFAULT '10',
  `charm_current` int(11) NOT NULL DEFAULT '10',
  `charm_max` int(11) NOT NULL DEFAULT '10',
  `attack` int(11) NOT NULL DEFAULT '0',
  `defence` int(11) NOT NULL DEFAULT '0',
  `encounter` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `characters` WRITE;
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;

INSERT INTO `characters` (`id`, `zone`, `name`, `rank`, `level`, `exp`, `marks`, `items`, `quests_current`, `quests_max`, `guts_current`, `guts_max`, `wits_current`, `wits_max`, `charm_current`, `charm_max`, `attack`, `defence`, `encounter`, `created_at`)
VALUES
	(1,'fields','TestPlayer','',23,119,860,'[]',95,105,13,15,15,15,14,14,5,8,'{}','2019-06-14 17:51:21'),
	(2,'fields','John Doe','',1,0,0,'[]',100,100,10,10,10,10,10,10,0,0,'{}','2019-06-25 17:25:21');

/*!40000 ALTER TABLE `characters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table encounters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `encounters`;

CREATE TABLE `encounters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `descriptions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `encounters` WRITE;
/*!40000 ALTER TABLE `encounters` DISABLE KEYS */;

INSERT INTO `encounters` (`id`, `name`, `key`, `descriptions`)
VALUES
	(1,'Fields Quest','fields.quest','[\"You are hunting a rabbit through the grass when suddenly you come across a {monster.name}.\",\"You\'re quietly minding your own business, when suddenly you get ambushed by a {monster.name}!\"]');

/*!40000 ALTER TABLE `encounters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table encounters_monsters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `encounters_monsters`;

CREATE TABLE `encounters_monsters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `encounter_key` varchar(255) NOT NULL DEFAULT '',
  `monster_key` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `encounters_monsters` WRITE;
/*!40000 ALTER TABLE `encounters_monsters` DISABLE KEYS */;

INSERT INTO `encounters_monsters` (`id`, `encounter_key`, `monster_key`)
VALUES
	(1,'fields.quest','giant_rat'),
	(2,'fields.quest','goblin');

/*!40000 ALTER TABLE `encounters_monsters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `attack_min` int(11) NOT NULL DEFAULT '0',
  `attack_max` int(11) NOT NULL DEFAULT '0',
  `defence_min` int(11) NOT NULL DEFAULT '0',
  `defence_max` int(11) NOT NULL DEFAULT '0',
  `skill_min` int(11) NOT NULL DEFAULT '0',
  `skill_max` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;

INSERT INTO `items` (`id`, `name`, `key`, `attack_min`, `attack_max`, `defence_min`, `defence_max`, `skill_min`, `skill_max`)
VALUES
	(1,'Buckler','buckler',0,0,1,3,0,0),
	(2,'Short Dirk','short_dirk',2,5,0,0,1,4),
	(3,'Leather Tunic','leather_tunic',0,0,3,9,0,0),
	(4,'Leather Boots','leather_boots',0,0,1,2,0,0);

/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table monsters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monsters`;

CREATE TABLE `monsters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `action_attack` tinyint(1) NOT NULL DEFAULT '1',
  `action_flee` tinyint(1) NOT NULL DEFAULT '1',
  `action_bribe` tinyint(1) NOT NULL DEFAULT '0',
  `action_feed` tinyint(1) NOT NULL DEFAULT '0',
  `action_trade` tinyint(1) NOT NULL DEFAULT '0',
  `weapon` varchar(255) NOT NULL,
  `armour` varchar(255) NOT NULL,
  `guts_min` int(11) NOT NULL DEFAULT '1',
  `guts_max` int(11) NOT NULL DEFAULT '1',
  `wits_min` int(11) NOT NULL DEFAULT '0',
  `wits_max` int(11) NOT NULL DEFAULT '0',
  `charm_min` int(11) NOT NULL DEFAULT '0',
  `charm_max` int(11) NOT NULL DEFAULT '0',
  `attack_min` int(11) NOT NULL DEFAULT '0',
  `attack_max` int(11) NOT NULL DEFAULT '0',
  `defence_min` int(11) NOT NULL DEFAULT '0',
  `defence_max` int(11) NOT NULL DEFAULT '0',
  `exp_min` int(11) NOT NULL DEFAULT '0',
  `exp_max` int(11) NOT NULL DEFAULT '0',
  `marks_min` int(11) NOT NULL DEFAULT '0',
  `marks_max` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `monsters` WRITE;
/*!40000 ALTER TABLE `monsters` DISABLE KEYS */;

INSERT INTO `monsters` (`id`, `name`, `key`, `action_attack`, `action_flee`, `action_bribe`, `action_feed`, `action_trade`, `weapon`, `armour`, `guts_min`, `guts_max`, `wits_min`, `wits_max`, `charm_min`, `charm_max`, `attack_min`, `attack_max`, `defence_min`, `defence_max`, `exp_min`, `exp_max`, `marks_min`, `marks_max`)
VALUES
	(1,'Giant Rat','giant_rat',1,1,0,1,0,'Sharp Claws','Mangy Fur',2,8,6,11,0,2,1,2,0,3,5,7,2,9),
	(2,'Goblin','goblin',1,1,1,0,0,'Short Dirk','Grubby Mail',4,6,4,8,1,4,1,4,2,4,4,9,4,6);

/*!40000 ALTER TABLE `monsters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `character` varchar(255) NOT NULL DEFAULT '',
  `quotes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;

INSERT INTO `shops` (`id`, `name`, `key`, `character`, `quotes`)
VALUES
	(1,'Charter House of the Dragon Guard','castle.guild','Alexandre Dumas','[\"Ah good! You have spirit!\",\"Are you as good as Queen Beth?\",\"Aspire to nobility\",\"Fight for the Queen\",\"Follow your heart\",\"For Queen and Country!\",\"I am old, but not blind\",\"I am old, but not deaf\",\"Practice and train\"]'),
	(2,'Healer\'s Tower','fields.healer','Elden Bishop','[\"Alms for the poor?\",\"Be brake like Queen Beth\",\"Can you spare some marks?\",\"Care for a massage?\",\"How may I aid thee?\",\"Let me heal thy aches\",\"Please let me help\",\"Share with the poor\",\"Thou art distressed\",\"Thou art disturbed\",\"Tithe for thy soul\"]'),
	(3,'Queen Beth\'s Court','castle.court','Queen Beth','[\"Give me a good jape\",\"Give me a good reason\",\"Have you see Queen Beth?\",\"OFF WITH HIS HEAD!\",\"Show me something special\",\"Tell me a story\",\"What a clever little man\",\"Why should I listen?\",\"Where have you been?\",\"You are boring me\"]');

/*!40000 ALTER TABLE `shops` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `token`)
VALUES
	(1,'joe','XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=','1');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table zones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `tiles` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `zones` WRITE;
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;

INSERT INTO `zones` (`id`, `name`, `key`, `tiles`)
VALUES
	(1,'The Central Courtyard of Draken Keep','castle','[{\"position\":1,\"name\":\"Clan Hall\",\"to\":{\"type\":\"shop\",\"key\":\"castle.clans\"}},{\"position\":2,\"name\":\"Royal Court\",\"to\":{\"type\":\"shop\",\"key\":\"castle.court\"}},{\"position\":3,\"name\":\"Dragon Guard\",\"to\":{\"type\":\"shop\",\"key\":\"castle.guild\"}},{\"position\":4,\"name\":\"Towne Gate\",\"to\":{\"type\":\"zone\",\"key\":\"town\"}},{\"position\":5,\"name\":\"Post Office\",\"to\":{\"type\":\"shop\",\"key\":\"castle.post_office\"}},{\"position\":6,\"name\":\"Dungeons\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"castle.dungeons\"}}]'),
	(2,'Welcome to the Fields','fields','[{\"position\":1,\"name\":\"Towne Road\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"town\"}},{\"position\":2,\"name\":\"Quest\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"fields.quest\"}},{\"position\":3,\"name\":\"Healer\'s Tower\",\"to\":{\"type\":\"shop\",\"key\":\"fields.healer\"}},{\"position\":4,\"name\":\"Forest Road\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"forest\"}},{\"position\":5,\"name\":\"Goblin Mound\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"goblin_mound\"}},{\"position\":6,\"name\":\"Camp\",\"to\":{\"type\":\"sleep\",\"key\":\"camp\"}}]'),
	(3,'The Depths of the Arcane Forest','forest','[{\"position\":1,\"name\":\"Mountain Trail\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"mountains\"}},{\"position\":2,\"name\":\"Quest\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"forest.quest\"}},{\"position\":3,\"name\":\"Fields Road\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"fields\"}},{\"position\":4,\"name\":\"Shortleg\'s Smithy\",\"to\":{\"type\":\"shop\",\"key\":\"forest.smith\"}},{\"position\":5,\"name\":\"Camp\",\"to\":{\"type\":\"sleep\",\"key\":\"camp\"}},{\"position\":6,\"name\":\"The Guild\",\"to\":{\"type\":\"shop\",\"key\":\"forest.guild\"}}]'),
	(4,'Welcome to the Goblin Mound','goblin_mound','[{\"position\":1,\"name\":\"To the Fields\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"fields\"}},{\"position\":2,\"name\":\"Dark Vortex\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"goblin_mound.vortex\"}},{\"position\":3,\"name\":\"Gobble Inn\",\"to\":{\"type\":\"shop\",\"key\":\"goblin_mound.inn\"}},{\"position\":4,\"name\":\"Throne Room\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"goblin_mound.throne_room\"}},{\"position\":5,\"name\":\"Warrens\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"goblin_mound.warrens\"}},{\"position\":6,\"name\":\"Treasury\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"goblin_mound.treasury\"}}]'),
	(5,'Market Lane in Salamander Townshippe','market_lane','[{\"position\":1,\"name\":\"Suitor\'s Armour\",\"to\":{\"type\":\"shop\",\"key\":\"market_lane.armour\"}},{\"position\":2,\"name\":\"Hire a boat ($50k)\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"sea\"}},{\"position\":3,\"name\":\"Slick\'s Den\",\"to\":{\"type\":\"shop\",\"key\":\"market_lane.den\"}},{\"position\":4,\"name\":\"Silver Storage\",\"to\":{\"type\":\"shop\",\"key\":\"market_lane.storage\"}},{\"position\":5,\"name\":\"Main Street\",\"to\":{\"type\":\"zone\",\"key\":\"town\"}}]'),
	(6,'High Crags of the Fenris Mountains','mountains','[{\"position\":1,\"name\":\"Djinni\'s Magic\",\"to\":{\"type\":\"shop\",\"key\":\"mountains.magic\"}},{\"position\":2,\"name\":\"Quest\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"mountains.quest\"}},{\"position\":3,\"name\":\"Gem Exchange\",\"to\":{\"type\":\"shop\",\"key\":\"mountains.gems\"}},{\"position\":4,\"name\":\"Camp\",\"to\":{\"type\":\"sleep\",\"key\":\"camp\"}},{\"position\":5,\"name\":\"Abandoned Mine\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"mountains.mine\"}},{\"position\":6,\"name\":\"Forest Trail\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"forest\"}}]'),
	(7,'The Sea of Tranquility','sea','[{\"position\":1,\"name\":\"Hie Brasil\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"sea.hie_brasil\"}},{\"position\":2,\"name\":\"Shangala\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"sea.shangala\"}},{\"position\":3,\"name\":\"Azteca\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"sea.azteca\"}},{\"position\":4,\"name\":\"Voyage Home\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"market_lane\"}},{\"position\":5,\"name\":\"Go Fish\",\"quests\":1,\"to\":{\"type\":\"combat\",\"key\":\"sea.quest\"}},{\"position\":6,\"name\":\"Seaside Diner\",\"to\":{\"type\":\"shop\",\"key\":\"sea.inn\"}}]'),
	(8,'Main Street in Salamander Townshippe','town','[{\"position\":1,\"name\":\"Keeper\'s Tavern\",\"to\":{\"type\":\"shop\",\"key\":\"town.tavern\"}},{\"position\":2,\"name\":\"Market Lane\",\"to\":{\"type\":\"zone\",\"key\":\"market_lane\"}},{\"position\":3,\"name\":\"Castle Gate\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"castle\"}},{\"position\":4,\"name\":\"Smith\'s Weapons\",\"to\":{\"type\":\"shop\",\"key\":\"town.armour\"}},{\"position\":5,\"name\":\"Trade Shoppe\",\"to\":{\"type\":\"shop\",\"key\":\"town.trader\"}},{\"position\":6,\"name\":\"Leave Towne\",\"quests\":1,\"to\":{\"type\":\"zone\",\"key\":\"fields\"}}]');

/*!40000 ALTER TABLE `zones` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
